#pragma once

#include <math.h>
#include "AudioSignal.h"
#include "FunctionTable.h"

class SG303Filter
{
public:
	inline SG303Filter(float cf = 100., float rez = 0.) :d1a(0.), d2a(0.), d1b(0.), d2b(0.) {
		cutoff = 0;
		setCutoff(cf);
		setResonance(rez);
	}

	inline ~SG303Filter() {}

	inline SG303Filter & reset() {
		d1a = d2a = d1b = d2b = 0;
		return *this;
	}
 
	SG303Filter &  setCutoff(float f  /* Hz */){
		if (cutoff != f) {
			cutoff = f;
			f1a = 2. * sin(pi * (f / signalSampleRate));
			f1b = 2. * sin(pi * ((f*1.01) / signalSampleRate));
		}
		return *this;
	}

	inline SG303Filter & setResonance(float r  /* 0. to 1.+ */) {
		//resonance squared to accentuate control of low q values /(similar mapping to 303)
		q1 = 1. / (1. + (r*r) *12.);
		return *this;
	}

	inline void filter(const Sample inSamp) {
		lpa = f1a*d1a + d2a;
		hp = inSamp - lpa - q1*d1a;
		bp = f1a*hp + d1a;
		d2a = lpa;
		d1a = bp;

		clip(d2a);
		clip(d1a);

		lpb = f1b*d1b + d2b;
		hp = lpa - lpb - q1*d1b;
		bp = f1b*hp + d1b;
		d2b = lpb;
		d1b = bp;

		clip(d2b);
		clip(d1b);
	}
	Sample highPass(Sample in) { filter(in); return hp; }
	Sample lowPass(Sample in) { filter(in); return lpb; }
	Sample bandPass(Sample in) { filter(in); return bp; }

	void operator=(SG303Filter &flt) {
		cutoff = flt.cutoff;
		f1a = flt.f1a;
		f1b = flt.f1b;
		q1 = flt.q1;
	}

private:
	float cutoff;
	float f1a,f1b,q1;		//filter coefficients
	float d1a,d2a,d1b,d2b;	//filter delay samples
	float lpa, lpb, hp, bp;
};