#pragma once

#include "AudioSignal.h"

/* 
Public source code by alex@smartelectronix.com 
Simple example of implementation of formant filter 
Vowelnum can be 0,1,2,3,4 <=> A,E,I,O,U 
Good for spectral rich input like saw or square 
*/ 
#define N_FORMANT_COEFFS	11

const static double formantCoeff[][N_FORMANT_COEFFS]= {
	{ 8.11044e-06,	8.943665402,	-36.83889529,	92.01697887,	-154.337906,	181.6233289,
	-151.8651235,	89.09614114,	-35.10298511,	8.388101016,	-0.923313471	},	// A

	{4.36215e-06,	8.90438318,		-36.55179099,	91.05750846,	-152.422234,	179.1170248,
	-149.6496211,	87.78352223,	-34.60687431,	8.282228154,	-0.914150747	},	// E

	{ 3.33819e-06,	8.893102966,	-36.49532826,	90.96543286,	-152.4545478,	179.4835618,
	-150.315433,	88.43409371,	-34.98612086,	8.407803364,	-0.932568035	},	// I

	{1.13572e-06,	8.994734087,	-37.2084849,	93.22900521,	-156.6929844,	184.596544,
	-154.3755513,	90.49663749,	-35.58964535,	8.478996281,	-0.929252233	},	// O

	{4.09431e-07,	8.997322763,	-37.20218544,	93.11385476,	-156.2530937,	183.7080141,
	-153.2631681,	89.59539726,	-35.12454591,	8.338655623,	-0.910251753	}	// U
};

class FormantFilter {
public:
	FormantFilter()
	{
		reset();
	}

	~FormantFilter()
	{
	}

	inline void reset()
	{
		vowelCoeff[0] = 1;
		for (int i=1; i<N_FORMANT_COEFFS; i++) {
			vowelCoeff[i] = 0;
		}
		for (int i=0; i<10; i++) {
			memory[i] = 0;
		}
	}


	inline void setVowel(int vowel)
	{
		if (vowel < 0) vowel = 0; else if (vowel > 4) vowel = 4;
		for (int i=0; i<N_FORMANT_COEFFS; i++) {
			vowelCoeff[i] = formantCoeff[vowel][i];
		}
	}

	inline void setVowel(float vowel)
	{
		if (vowel < 0) vowel = 0; else if (vowel > 1) vowel = 1;
		float ind = vowel * 4;
		int lind = (int)floor(ind);
		int rind = (int)ceil(ind);
		float pind = ind - lind;
		for (int i=0; i<N_FORMANT_COEFFS; i++) {
			vowelCoeff[i] = (1-pind)*formantCoeff[lind][i] + pind*formantCoeff[rind][i];
		}
	}

	Sample filter(Sample in)
	{ 
		Sample res=
				vowelCoeff[0] * in +
				vowelCoeff[1] * memory[0] +
				vowelCoeff[2] * memory[1] +
				vowelCoeff[3] * memory[2] +
				vowelCoeff[4] * memory[3] +
				vowelCoeff[5] * memory[4] +
				vowelCoeff[6] * memory[5] +
				vowelCoeff[7] * memory[6] +
				vowelCoeff[8] * memory[7] +
				vowelCoeff[9] * memory[8] +
				vowelCoeff[10] * memory[9];
		
		memory[9]= memory[8]; 
		memory[8]= memory[7]; 
		memory[7]= memory[6]; 
		memory[6]= memory[5]; 
		memory[5]= memory[4]; 
		memory[4]= memory[3]; 
		memory[3]= memory[2]; 
		memory[2]= memory[1]; 
		memory[1]= memory[0]; 
		memory[0]=(float) res;
		
		if (res < -0.99) {
			res = -0.99;
		} else if (res > 0.99) {
			res = 0.99;
		}

		return res; 
	}

	inline void operator=(FormantFilter&flt)
	{
		for (int i=1; i<N_FORMANT_COEFFS; i++) {
			vowelCoeff[i] = flt.vowelCoeff[i];
		}
	}

protected:
	double vowelCoeff[N_FORMANT_COEFFS];
	double memory[10];
};