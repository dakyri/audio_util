/*
 * Phaser.h
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */
#pragma once
#include "audio_util.h"

class Phaser {
	const int BOTTOM_FREQ = 100;
public:
	Phaser() {
		setParameters(0.1, 0.8, 0, 0, 1);
	}
	virtual ~Phaser() {}

	void initialize() {}
	void reset() {}

	void setParameters(float _rate, float _width, float _fb, int _stages, float _mix) {
		setRate(_rate);
		setWidth(_width);
		setFeedback(_fb);
		setStages(_stages);
		setMix(_mix);
	}

	void setRate(float rate) {
		paramSweepRate = rate;
		setSweep();
	}

	void setWidth(float v) {
		paramWidth = v;
		setSweep();
	}

	void setFeedback (float v) {
		paramFeedback = v;
		feedback = v;
	}

	/*!
	 * Expects input from 1,2,3,4,5
	 */
	void setStages(int v)
	{
		paramStages = v;
		stages = v;
	}

	void Phaser::setMix(float v)
	{
		paramMix = v;
	}

	void setSweep(void) {
		// calc the actual sweep rate
		double rate = pow(10.0, (double)paramSweepRate);
		rate -= 1.0;
		rate *= 1.1f;
		rate += 0.1f;
		sweepRate = rate;
		double range = paramWidth * 6.0f;

		// do the rest of the inits here just for fun
		wp = minwp = (pi * BOTTOM_FREQ) / (double)signalSampleRate;
		maxwp = (pi * BOTTOM_FREQ * range) / (double)signalSampleRate;

		// figure out increment to multiply by each time
		sweepFactor = pow((double)range, (double)(sweepRate / (signalSampleRate / 2.0)));

		// init the all pass line
		lx1 = ly1 =
			lx2 = ly2 =
			lx3 = ly3 =
			lx4 = ly4 =
			lx5 = ly5 =
			lx6 = ly6 =
			lx7 = ly7 =
			lx8 = ly8 =
			lx9 = ly9 =
			lx10 = ly10 = 0.0f;
	}



	void apply(float *signal, long sampleFrames) {
		/*	float* in1 = inputs[0];
		float* in2 = inputs[1];
		float* out1 = outputs[0];
		float* out2 = outputs[1];
		*/
		switch (stages) {
		case 1:
			for (int i = 0, j = 0; i < sampleFrames; i++, j += 2) {
				double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

														   // get input value
				float in1 = signal[j];
				float in2 = signal[j + 1];
				float inval = (in1 + in2) / 2.0f;
				double inmix = inval + feedback * feedbackPhase * ly2;

				// run thru the all pass filters
				ly1 = coef * (ly1 + inmix) - lx1;		// do 1st filter
				lx1 = inmix;
				ly2 = coef * (ly2 + ly1) - lx2;			// do 2nd filter
				lx2 = ly1;

				// develop output mix
				/*			out1[i] = (float)PIN(mixLeftDry * inval + mixLeftWet * ly2,-0.99,0.99);
				out2[i] = (float)PIN(mixRightDry * inval + mixRightWet * ly2,-0.99,0.99);*/
				signal[j] = (float)audio_util::pin(in1 + paramMix*ly2, -0.99, 0.99);
				signal[j + 1] = (float)audio_util::pin(in2 + paramMix*ly2, -0.99, 0.99);

				// step the sweep
				wp *= sweepFactor;                    // adjust freq of filters
				if (wp > maxwp || wp < minwp) {
					sweepFactor = 1.0 / sweepFactor;	// reverse
				}
			}
			break;

		case 2:
		default:
			for (int i = 0, j = 0; i < sampleFrames; i++, j += 2) {
				double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

														   // get input value
				float in1 = signal[j];
				float in2 = signal[j + 1];
				float inval = (in1 + in2) / 2.0f;
				double inmix = inval + feedback * feedbackPhase * ly4;

				// run thru the all pass filters
				ly1 = coef * (ly1 + inmix) - lx1;		// do 1st filter
				lx1 = inmix;
				ly2 = coef * (ly2 + ly1) - lx2;			// do 2nd filter
				lx2 = ly1;
				ly3 = coef * (ly3 + ly2) - lx3;			// do 3rd filter
				lx3 = ly2;
				ly4 = coef * (ly4 + ly3) - lx4;			// do 4th filter
				lx4 = ly3;

				/*			out1[i] = (float)tanh(mixLeftDry * inval + mixLeftWet * ly2);
				out2[i] = (float)tanh(mixRightDry * inval + mixRightWet * ly2);*/
				signal[j] = (float)tanh(inval + ly2);
				signal[j + 1] = (float)tanh(inval + ly2);

				// step the sweep
				wp *= sweepFactor;                    // adjust freq of filters
				if (wp > maxwp || wp < minwp) {
					sweepFactor = 1.0 / sweepFactor;	// reverse
				}
			}
			break;
		case 3:
			for (int i = 0, j = 0; i < sampleFrames; i++, j += 2) {
				double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

														   // get input value
				float in1 = signal[j];
				float in2 = signal[j + 1];
				float inval = (in1 + in2) / 2.0f;
				double inmix = inval + feedback * feedbackPhase * ly6;

				// run thru the all pass filters
				ly1 = coef * (ly1 + inmix) - lx1;		// do 1st filter
				lx1 = inmix;
				ly2 = coef * (ly2 + ly1) - lx2;			// do 2nd filter
				lx2 = ly1;
				ly3 = coef * (ly3 + ly2) - lx3;			// do 3rd filter
				lx3 = ly2;
				ly4 = coef * (ly4 + ly3) - lx4;			// do 4th filter
				lx4 = ly3;
				ly5 = coef * (ly5 + ly4) - lx5;			// do 5th filter
				lx5 = ly4;
				ly6 = coef * (ly6 + ly5) - lx6;			// do 6th filter
				lx6 = ly5;

				/*			out1[i] = (float)tanh(mixLeftDry * inval + mixLeftWet * ly2);
				out2[i] = (float)tanh(mixRightDry * inval + mixRightWet * ly2);*/
				signal[j] = (float)tanh(in1 + ly2);
				signal[j + 1] = (float)tanh(in2 + ly2);

				// step the sweep
				wp *= sweepFactor;                    // adjust freq of filters
				if (wp > maxwp || wp < minwp) {
					sweepFactor = 1.0 / sweepFactor;	// reverse
				}
			}
			break;

		case 4:
			for (int i = 0, j = 0; i < sampleFrames; i++, j += 2) {
				double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

														   // get input value
				float in1 = signal[j];
				float in2 = signal[j + 1];
				float inval = (in1 + in2) / 2.0f;
				double inmix = inval + feedback * feedbackPhase * ly8;

				// run thru the all pass filters
				ly1 = coef * (ly1 + inmix) - lx1;		// do 1st filter
				lx1 = inmix;
				ly2 = coef * (ly2 + ly1) - lx2;			// do 2nd filter
				lx2 = ly1;
				ly3 = coef * (ly3 + ly2) - lx3;			// do 3rd filter
				lx3 = ly2;
				ly4 = coef * (ly4 + ly3) - lx4;			// do 4th filter
				lx4 = ly3;
				ly5 = coef * (ly5 + ly4) - lx5;			// do 5th filter
				lx5 = ly4;
				ly6 = coef * (ly6 + ly5) - lx6;			// do 6th filter
				lx6 = ly5;
				ly7 = coef * (ly7 + ly6) - lx7;			// do 7th filter
				lx7 = ly6;
				ly8 = coef * (ly8 + ly7) - lx8;			// do 8th filter
				lx8 = ly7;

				/*			out1[i] = (float)tanh(mixLeftDry * inval + mixLeftWet * ly2);
				out2[i] = (float)tanh(mixRightDry * inval + mixRightWet * ly2);*/
				signal[j] = (float)tanh(in1 + ly2);
				signal[j + 1] = (float)tanh(in2 + ly2);

				// step the sweep
				wp *= sweepFactor;                    // adjust freq of filters
				if (wp > maxwp || wp < minwp) {
					sweepFactor = 1.0 / sweepFactor;	// reverse
				}
			}
			break;
		case 5:
			for (int i = 0, j = 0; i < sampleFrames; i++, j += 2) {
				double coef = (1.0 - wp) / (1.0 + wp);     // calc coef for current freq

														   // get input value
														   // get input value
				float in1 = signal[j];
				float in2 = signal[j + 1];
				float inval = (in1 + in2) / 2.0f;
				double inmix = inval + feedback * feedbackPhase * ly10;

				// run thru the all pass filters
				ly1 = coef * (ly1 + inmix) - lx1;			// do 1st filter
				lx1 = inmix;
				ly2 = coef * (ly2 + ly1) - lx2;			// do 2nd filter
				lx2 = ly1;
				ly3 = coef * (ly3 + ly2) - lx3;			// do 3rd filter
				lx3 = ly2;
				ly4 = coef * (ly4 + ly3) - lx4;			// do 4th filter
				lx4 = ly3;
				ly5 = coef * (ly5 + ly4) - lx5;			// do 5th filter
				lx5 = ly4;
				ly6 = coef * (ly6 + ly5) - lx6;			// do 6th filter
				lx6 = ly5;
				ly7 = coef * (ly7 + ly6) - lx7;			// do 7th filter
				lx7 = ly6;
				ly8 = coef * (ly8 + ly7) - lx8;			// do 8th filter
				lx8 = ly7;
				ly9 = coef * (ly9 + ly8) - lx9;			// do 9th filter
				lx9 = ly8;
				ly10 = coef * (ly10 + ly9) - lx10;			// do 10th filter
				lx10 = ly9;

				/*			out1[i] = (float)tanh(mixLeftDry * inval + mixLeftWet * ly2);
				out2[i] = (float)tanh(mixRightDry * inval + mixRightWet * ly2);*/
				signal[j] = (float)tanh(in1 + ly2);
				signal[j + 1] = (float)tanh(in2 + ly2);

				// step the sweep
				wp *= sweepFactor;                    // adjust freq of filters
				if (wp > maxwp || wp < minwp) {
					sweepFactor = 1.0 / sweepFactor;	// reverse
				}
			}
			break;
		}

	}

protected:
	float paramSweepRate;		// 0.0-1.0 passed in
	float paramWidth;			// ditto
	float paramFeedback;		// ditto
	int paramStages;			// ditto
	float paramMix;		// ditto
	double sweepRate;			// actual calc'd sweep rate
	double width;				// 0-100%
	double feedback;			// 0.0 to 1.0
	double feedbackPhase;		// -1.0 or 1.0
	int stages;				// calc'd # of stages 2-10

	double wp;					// freq param for equation
	double minwp;
	double maxwp;
	double sweepFactor;		// amount to multiply the freq by with each sample

	// the all pass line
	double lx1;
	double ly1;
	double lx2;
	double ly2;
	double lx3;
	double ly3;
	double lx4;
	double ly4;
	double lx5;
	double ly5;
	double lx6;
	double ly6;
	double lx7;
	double ly7;
	double lx8;
	double ly8;
	double lx9;
	double ly9;
	double lx10;
	double ly10;
};
