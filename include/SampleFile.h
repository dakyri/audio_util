#pragma once

#include <cstdint>
#include <fstream>

#include "audio_util.h"

enum SammpleFileError {
	NOT_ERROR = 1,
	HEADER_OK = 0,
	AN_ERROR = -1,
	FILE_ERROR = -2,
	OBSCURE_ERROR = -3,
	MALFORM_ERROR = -4,
	UNKNOWN_FILETYPE = -5,
	UNUSUAL_OPENMODE = -6,
	PERMISSION_DENIED = -7,
	FILE_EXISTS = -8,
	NO_MORE_FDS = -9,
	FILE_NOT_FOUND = -10
};


// form chunk header

#define FormID		'FORM'
#define AIFFckID	'AIFF'

struct FormChunkHeader
{
	uint32_t ckID;
	uint32_t ckSize;
};

struct FormAIFFHeader
{
	uint32_t chunkType;
	uint32_t chunkLen;
	uint32_t formType;
};

#define CommonID    'COMM'  /* ckID for Common Chunk */ 

struct  CommonChunk { 
    uint16_t numChannels;
    uint16_t numSampleFrames1;
    uint16_t numSampleFrames2;
    uint16_t sampleSize;
    uint8_t sampleRate[10];	
};  

#define SoundDataID 'SSND'  /* ckID for Sound Data Chunk */ 

struct  SoundDataChunk { 
    uint32_t offset; 
    uint32_t blockSize;
//    uint32			soundData[]; 

};   

#define MarkerCkID    'MARK'  /* ckID for Marker Chunk */ 

typedef int16_t MarkerId; 

struct  Marker{ 
    MarkerId id; 
    uint32_t position; 
//	char markerName[];	// pascal ?? string 
}; 

struct  MarkerChunk{ 
    uint16_t numMarkers; 
//	Marker Markers[]; 
}; 


#define InstrumentID    'INST'  /* ckID for Instrument Chunk */ 

#define NoLooping               0 
#define ForwardLooping          1 
#define ForwardBackwardLooping  2 

struct  Loop{ 
    uint16_t playMode; 
    MarkerId beginLoop; 
    MarkerId endLoop; 
}; 

struct  InstrumentChunk{ 
    int8_t baseNote; 
	int8_t detune;
	int8_t lowNote;
	int8_t highNote;
	int8_t lowVelocity;
	int8_t highVelocity;
    int16_t gain; 
    Loop sustainLoop; 
    Loop releaseLoop; 
}; 


#define MIDIDataID  'MIDI'  /* ckID for MIDI Data Chunk */ 

struct  MIDIDataChunk { 
//	uint8_t MIDIdata[]; 
}; 

#define AudioRecordingID  'AESD'        /* ckID for Audio Recording Chunk. */ 

struct  AudioRecordingChunk{ 
	uint8_t AESChannelStatusData[24]; 
}; 

#define ApplicationSpecificID  'APPL'   /* ckID for Application Specific Chunk.     */ 

struct  ApplicationSpecificChunk { 
    uint32_t applicationSignature; 
//	char data[]; 
}; 


#define CommentID       'COMT'  /* ckID for Comments Chunk.  */ 

struct  Comment{ 
	uint32_t timeStamp; 
	int16_t markID; 
	uint16_t count; 
//	char text[]; 
}; 

struct  CommentsChunk{ 
	uint16_t numComments; 
//	Comment comments[]; 
}; 

#define NameID          'NAME'  /* ckID for Name Chunk.  */ 
#define AuthorID        'AUTH'  /* ckID for Author Chunk.  */ 
#define CopyrightID     '(c) '  /* ckID for Copyright Chunk.  */ 
#define AnnotationID    'ANNO'  /* ckID for Annotation Chunk.  */ 

struct TextChunk { 
//	char text[]; 
}; 

// riff wave header

struct RiffChunkHeader
{
	uint32_t chunkType;
	uint32_t chunkLen;
};

struct RiffWaveCommonHeader
{
	uint16_t format;
	uint16_t channels;
	uint32_t sampleRate;
	uint32_t byteRate;
	uint16_t blockAlign;
};

struct RiffWavePCMHeader
{
	uint16_t bitsPerSample;
};

enum {
	RiffWaveFmtMSPCM = 1,
	RiffWaveFmtIBMMuLaw = 0x101,
	RiffWaveFmtIBMALaw = 0x102,
	RiffWaveFmtIBMAPCM = 0x103
};

// base sample file class

class SampleFile
{
public:

	enum FileType {
		NO_TYPE = 0,
		RAW_TYPE = 1,
		WAVE_TYPE = 2,
		AIFF_TYPE = 3,
		SND_TYPE = 4,
		AIFC_TYPE = 5,	// just a compressed mode for aif
		MP3_TYPE = 6,	// wish list
		MP4_TYPE = 7,	// even wishier
		OGG_TYPE = 8,	// not too optimistic
		AU_TYPE = 9,	// probably should already have done this.
		AAC_TYPE = 10,	// mucky but not impossible
		WMA_TYPE = 11	// exceptionally optimistic
	};

	SampleFile(FileType typ=NO_TYPE, ushort nchan=2, ushort dize=2, ulong sr=44100, std::ios_base::openmode mode=std::ios_base::in);
	SampleFile(char *, std::ios_base::openmode mode= std::ios_base::in);
	~SampleFile();
						
	void SetFormat(ushort nchan, ushort ushort, ulong sr);

	status_t SetTo(const char *p, std::ios_base::openmode mode= std::ios_base::in);

	status_t ReadHeaderInfo();
	status_t WriteHeaderInfo();
	status_t ProcessHeaderInfo();
	status_t TryAiffFile();
	status_t TrySndFile();
	status_t TryWaveFile();
	status_t TryRawFile();
	status_t WriteWaveHeader();
	status_t WriteAiffHeader();
	status_t WriteSndHeader();

	void NormalizeInput(char * buf, std::streamsize nf);
	void NormalizeOutput(float * buf, std::streamsize nf);
	void NormalizeOutput(short * buf, std::streamsize nf);
	void NormalizeOutputCpy(char *buf, float *b2, std::streamsize nf);
	std::streamsize NormalizeInputCpy(float *buf, char *b2, std::streamsize nf);
	std::streamsize NormalizeInputCpy(short *buf, char *b2, std::streamsize nf);

	std::streamoff Frame() { return (Position() - sampleDataStart) / (sampleSize * nChannels); }
	std::streamoff FrameToOffset(std::streamsize fr) { return fr * (sampleSize * nChannels) + sampleDataStart; }
	std::streamoff OffsetToFrame(std::streamsize fr);
	std::streamoff FrameToByte(std::streamsize fr) { return fr * (sampleSize * nChannels); }
	std::streamoff ByteToFrame(std::streamsize nb) { return nb / (sampleSize * nChannels); }

	status_t GetLastError();

	void SetType(short typ);
	inline short Type() { return fileType; }
	status_t Plonk(short *buffer);
	status_t Finalize();

	inline std::streamsize ByteSize() { return nFrames*nChannels*sampleSize; }
	inline std::streampos Position() {
		return file.is_open()? file.tellg():0;
	}
	inline std::streamsize Write(void *buf, std::streamsize nb) {
		if (!file.is_open()) return -1;
		file.write((char*)buf, nb);
		return !file.fail()? nb: 0;
	}
	inline std::streamsize Read(void *buf, std::streamsize nb) {
		if (!file.is_open()) return -1;
		file.read((char*)buf, nb);
		return file.gcount();
	}
	inline void Seek(std::streamoff off, std::ios_base::seekdir way=std::ios_base::beg) {
		if (file.is_open()) { file.seekg(off, way); }
	}
	status_t GetSize(std::streamsize*);
	status_t SetSize(std::streamsize);

	inline status_t SeekToFrame(std::streamoff fr) { Seek(FrameToOffset(fr)); return OKIDOKI; }
	void operator=(const SampleFile&f);

	std::fstream file;

	uint32_t openMode;
	short nChannels;
	short sampleSize;
	std::streamsize nFrames;
	short fileType;
	short sampleType;
	std::streamoff sampleDataStart;
	long sampleRate;
	std::string path;
};