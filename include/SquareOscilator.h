#pragma once

#include "AudioSignal.h"

class SquareOscilator
{
public:
	SquareOscilator(float a = 1.): sample(-1), width(1), amp(a) {
		setFrequency(256);
	}

	inline void setWidth(float a) { width = (a<0 ? 0 : a>1 ? 1 : a); }
	inline void setAmplitude(float a) { amp = a; }
	inline void reset() { sample = -1; }

	inline void setFrequency(float freq, float sampleRate=signalSampleRate) {
		cycle = (sampleRate / freq);
		halfCycle = cycle / 2;
		up = (1.0 + 7.0*width)*halfCycle / 8.0;
	}
	
	inline Sample operator () () {
		float	val = 0;
		if (++sample < up)
			return amp;
		else if (sample >= halfCycle && sample <= halfCycle + up)
			return -amp;
		if (sample >= cycle)
			sample = -1;
		return val;
	}

	template <uint N> inline void operator () (Signal<N> &outSig) {
		for (unsigned long i = 0; i<maxFramesPerChunk; i++)
			outSig[i] = (*this)();
	}

private:
	long sample;
	float cycle;
	float amp;
	long up;
	long halfCycle;
	float width;
};
