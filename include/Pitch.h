#pragma once

#include <math.h>
#include <array>
#include "audio_util.h"
/*
 * Pitch.h
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 * rough adapt from a pd vst plugin
 * TODO: lots!!!
 */

class Pitch {
	static constexpr int kBufSize = 1024*(int(0.2 * 2 * signalSampleRate)/1024); // must be about 1/5 of a second at given sample rate

	const float kMinSweep = 0.015f;
	const float kMaxSweep = 0.1f;
	const float kSemiTone = 1.0594631f;

	const int kNumPitches = 25;
	const int kMinDelaySamples = 22;
public:
	Pitch(): mix(0.5f) {
		/*
		pPrograms[0] = new Pitch1Program( 0.515f, 0.48f, 0.0f, 0.1f, 0.0f, "Harmonizer");
		pPrograms[1] = new Pitch1Program( 0.5f, 0.0f, 0.0f, 0.1f, 0.0f, "Octave Up");
		pPrograms[2] = new Pitch1Program( 0.5f, 0.96f, 0.0f, 0.1f, 0.0f, "Octave Down");

		pPrograms[3] = new Pitch1Program( 0.53f, 0.48f, 0.0f, 0.1f, 0.0f, "Deep Harmonizer");
		pPrograms[4] = new Pitch1Program( 0.5f, 0.2f, 0.0f, 0.1f, 0.0f, "Fifth Up");
		pPrograms[5] = new Pitch1Program( 0.5f, 0.76f, 0.0f, 0.1f, 0.0f, "Fifth Down");
		pPrograms[6] = new Pitch1Program( 0.515f, 0.48f, 0.0f, 0.1f, 0.857f, "Stereo Harmonizer");
		pPrograms[7] = new Pitch1Program( 0.5f, 0.52f, 0.3f, 0.9f, 0.0f, "Descending Echoes");
		*/
		// make sure all instance vars are init'd to some known value
		paramFinePitch = 0.5f;
		paramCoarsePitch = 0.48f;
		paramFeedback = 0.0f;
		paramDelay = 0.0f;
		sweepRate = 0.2;
		feedback = 0.0;
		feedbackPhase = 1.0;
		sweepSamples = 0;
		delaySamples = 22;
		sweepInc = 0.0;
		setSweep();
		mix = 0;
		fp = 0;

		outval = 0.0f;
		buf.fill(0.0f);
	}

	virtual ~Pitch() { }

	/*! sets the fine pitch shift amount*/
	void setFine (float v) {
		paramFinePitch = v;
		setSweep();
	}

	/*! Maps 0.0-1.0 input to calculated width in samples from 0ms to 10ms*/
	void setCoarse (float v) {
		paramCoarsePitch = v;
		setSweep();
	}


	void setFeedback (float v) {
		paramFeedback = v;
		feedback = v;
	}

	/*! Map to the expect 0-100ms range.*/
	void setDelay (float v) {
		paramDelay = v;
		delaySamples = 0.1 * signalSampleRate * v; // convert incoming float to int sample count for up to 100ms of delay
		// pin to a minimum value so our sweep never collides with the filling pointer
		delaySamples = delaySamples < kMinDelaySamples ? kMinDelaySamples : delaySamples;
		setSweep();
	}

	void apply(Sample *signal, long sampleFrames) {
		// iterate thru sample frames
		for (int i = 0, j = 0; i < sampleFrames; i++, j += 2) {
			float in1 = signal[j];
			float in2 = signal[j + 1];

			// assemble mono input value and store it in circle queue
			double inval = (in1 + in2) / 2.0f;
			double inmix = inval + feedback * feedbackPhase * outval;

			buf[fp] = static_cast<Sample>(inmix);
			fp = (fp + 1) & (kBufSize - 1);

			// do the two taps
			outval = 0.0;

			// channel A build the two emptying pointers and do linear interpolation
			int ep1, ep2;
			double w1, w2;
			double ep = fp - delaySamples;
			ep -= sweepA;
			if (ep < kBufSize) {
				ep += kBufSize;
			}
			w2 = audio_util::modf(ep, ep1);
			ep1 &= (kBufSize - 1);
			ep2 = ep1 + 1;
			ep2 &= (kBufSize - 1);
			w1 = 1.0 - w2;
			double tapout = buf[ep1] * w1 + buf[ep2] * w2;
			double fade = sin((sweepA / sweepSamples) * pi);
			tapout *= fade;
			outval += tapout;

			// step the sweep
			sweepA += sweepInc;
			if (sweepA < 0) {
				sweepA = sweepSamples;
			}
			else if (sweepA >= sweepSamples) {
				sweepA = 0.0;
			}

			// channel B build the two emptying pointers and do linear interpolation
			ep = fp - delaySamples;
			ep -= sweepB;
			if (ep < kBufSize) {
				ep += kBufSize;
			}
			w2 = audio_util::modf(ep, ep1);
			ep1 &= (kBufSize);
			ep2 = ep1 + 1;
			ep2 &= (kBufSize - 1);
			w1 = 1.0 - w2;
			tapout = buf[ep1] * w1 + buf[ep2] * w2;
			fade = sin((sweepB / sweepSamples) * pi);
			tapout *= fade;
			outval += tapout;

			// step the sweep
			sweepB += sweepInc;
			if (sweepB < 0) {
				sweepB = sweepSamples;
			}
			else if (sweepB >= sweepSamples) {
				sweepB = 0.0;
			}

			signal[j] = (float)audio_util::pin((1-mix)*in1 + mix*outval, -0.99, 0.99);
			signal[j + 1] = (float)audio_util::pin((1 - mix)*in2 + mix*outval, -0.99, 0.99);

		} // end of loop iterating thru sample frames
	}

protected:
	/*!
	 * Sets up sweep based on pitch delta and delay as they're interrelated
	 * Assumes paramFinePitch, paramCoarsePitch, and delaySamples have all been set by
	 * setFine, setCoarse, and setDelay
	 */
	void setSweep(void) {
		// calc the total pitch delta
		double semiTones = 12.0 - round(paramCoarsePitch * kNumPitches);
		double fine = (2.0 * paramFinePitch) - 1.0;
		double pitchDelta = pow(kSemiTone, semiTones + fine);

		// see if we're increasing or decreasing
		increasing = pitchDelta >= 1.0;

		// calc the # of samples in the sweep, 15ms minimum, scaled up to 50ms for an octave up, and 32.5ms for an octave down
		double absDelta = fabs(pitchDelta - 1.0);
		sweepSamples = (kMinSweep + (kMaxSweep - kMinSweep) * absDelta) * signalSampleRate;

		// fix up the pitchDelta to become the sweepInc
		sweepInc = pitchDelta - 1.0;
		sweepInc = -sweepInc;

		// assign initial pointers
		sweepB = sweepSamples / 2.0;
		sweepA = increasing ? sweepSamples : 0.0;
	}

	float paramFinePitch;		// 0.0-1.0 passed in
	float paramCoarsePitch;	// ditto
	float paramFeedback;		// ditto
	float paramDelay;			// ditto

	float mix;				/*!< 0 (dry) .. 1 (wet) */
	double sweepRate;		/*!< actual calc'd sweep rate */
	double feedback;		/*!< 0.0 to 1.0 */
	double feedbackPhase;	/*!< -1.0 to 1.0 */
	double sweepSamples;	/*!< samples to use in sweep */
	double delaySamples;	/*!< number of samples to run behind filling pointer */
	double sweepA;			/*!< sweep position for channel A */
	double sweepB;			/*!< sweep position for channel B */
	std::array<Sample, kBufSize> buf;	/*!< stored sound */
	int fp;					/*!< fill/write pointer */
	double outval;			/*!< most recent output value (for feedback) */
	double sweepInc;		/*!< calculated by desired pitch deviation */
	bool increasing;		/*!< flag for pitch increasing/decreasing */
};
