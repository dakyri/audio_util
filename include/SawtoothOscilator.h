#pragma once

#include "AudioSignal.h"

class SawtoothOscilator
{
public:
	inline SawtoothOscilator(float amp = 1.) : currentValue(1.) {
		min = -amp;
		rval = 2 * amp;
		setFrequency(440);
	}
	
	inline SawtoothOscilator& setAmplitude(float amp) {
		currentValue /= inc;
		inc = (2 * amp) / (rval / inc);
		currentValue *= inc;
		min = -amp;
		rval = 2 * amp;
		return *this;
	}

	inline SawtoothOscilator & setFrequency(float freq, float sampleRate)
	{
		inc = rval / (sampleRate / freq);
		return *this;
	}

	inline SawtoothOscilator & setFrequency(float freq) {
		inc = rval / (signalSampleRate / freq);
		return *this;
	}

	inline SawtoothOscilator & reset() {
		currentValue = 1;
		return *this;
	}

	inline Sample operator () (void) {
		currentValue -= inc;
		if (currentValue < min)
			currentValue += rval;
		return currentValue;
	}

	template <uint N> inline void operator () (Signal<N> &outSig) {
		for (unsigned long i = 0; i<maxFramesPerChunk; i++)
			outSig[i] = (*this)();
	}

private:
	float currentValue;
	float inc;
	float min;
	float rval;
};