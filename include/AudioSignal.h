#pragma once

#include <stdlib.h>
#include <array>
#include "audio_util.h"


template <uint N> class Signal
{
public:
	Signal(Sample dc) { *this = dc; }
	Signal(Signal& sig) { *this = sig; }
	~Signal() {}
	
	inline Signal& operator = (Signal& sig) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] = sig[i];
		return *this;
	}

	inline Signal& operator = (const Sample dc) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] = dc;
		return *this;
	}

	inline Sample& operator [] (ulong i) { return sampArray[i]; }
	
	inline Signal& operator +=	(Signal& op) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] += op[i];
		return *this;
	}
	inline Signal& operator +=	(Sample op) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] += op;
		return *this;
	}
	inline Signal& operator -=	(Signal& op) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] -= op[i];
		return *this;
	}
	inline Signal& operator -=	(Sample op) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] -= op;
		return *this;
	}
	inline Signal& operator *=	(Signal& op) {
		for (unsigned long i = 0; i<N; i++)
			sampArray[i] *= op[i];
		return *this;
	}
	inline Signal& operator *=	(Sample op) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] *= op;
		return *this;
	}
	inline Signal& operator /=	(Signal& op) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] /= op[i];
		return *this;
	}
	inline Signal& operator /=	(Sample op) {
		for (ulong i = 0; i < N; i++)
			sampArray[i] /= op;
		return *this;
	}
	
	//multiply accumulate:
	inline Signal& mac( Signal s, Sample op ) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] += s[i] * op;
		return *this;
	}
	inline Signal& mac( Sample op, Signal s ) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] += s[i] * op;
		return *this;
	}
	inline Signal& mac( Signal s1, Signal s2 ) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] += s1[i] * s2[i];
		return *this;
	}
	
	//multiply assign
	inline Signal& mass( Signal s, Sample op ) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] = s[i] * op;
		return *this;
	}
	inline Signal& mass( Sample op, Signal s ) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] = s[i] * op;
		return *this;
	}
	inline Signal& mass( Signal s1, Signal s2 ) {
		for (ulong i = 0; i<N; i++)
			sampArray[i] = s1[i] * s2[i];
		return *this;
	}

	const uint length = N;
	
	inline static float vectorRate() { return signalSampleRate / N; }
	inline static float vectorDuration() { return float(1. / vectorRate()); }

	std::array<Sample, N> sampArray;
};
