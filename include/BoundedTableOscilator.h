#pragma once

#include "TableOscilator.h"

/*!
 * TableOscilator with ability to select start and end points in wavetable
 *
 * the oscilator retains ownership of the wave table. The mess is cleaned up by the parent.
 *
 * the wavetable should be non-null by the time initialization is done
 */
class BoundedTableOscilator : public TableOscilator
{
public:
	BoundedTableOscilator(FunctionTable *wavetable = nullptr, float phase = 0.)
		: TableOscilator() {
		setFunkTable(wavetable);
		setPhase(phase);
	}
	~BoundedTableOscilator() {}

	ulong Length() {
		return wavetable == nullptr || endPos < startPos ? 0 : startPos - endPos;
	}

	BoundedTableOscilator& setFunkTable(FunctionTable *wt) {
		if (wavetable != nullptr) {
			delete wavetable;
		}
		wavetable = wt;
		startPos = 0;
		endPos = (wt != nullptr) ? wt->Length() : 0;
		return *this;
	}

	BoundedTableOscilator& setPhase(float phase) { //0 <= phase <=1.
		phs = phase*Length();
		return *this;
	}

	BoundedTableOscilator& setFrequency(float freq, float sampleRate = signalSampleRate) {
		si = ((float)Length())*freq / sampleRate;
		return *this;
	}

	BoundedTableOscilator& setRelativeFrequency(float rFreq) {
		si = ((float)Length())*rFreq;
		return *this;
	}

	inline Sample operator () (void) {
		ulong idx = (ulong)phs;
		float frac = phs - idx;
		phs += si;
		if (phs > endPos)
			phs -= endPos - startPos;
		Sample nxtSample = (idx >= endPos) ? (*wavetable)[startPos] : (*wavetable)[idx + 1];
		return (*wavetable)[idx] + (nxtSample - (*wavetable)[idx])*frac;
	}

	inline Sample operator *(void) {
		ulong idx = (ulong)phs;
		float frac = phs - idx;
		Sample nxtSample = (idx >= endPos) ? (*wavetable)[startPos] : (*wavetable)[idx + 1];
		return (*wavetable)[idx] + ((*wavetable)[idx + 1] - (*wavetable)[idx])*frac;
	}

	template <uint N> void operator () (Signal<N> & outSig) {
		for (unsigned long i = 0; i < outSig.length; i++)
			outSig[i] = operator()();
	}

protected:
	ulong startPos;
	ulong endPos;
};