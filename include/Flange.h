/*
 * Flange.h
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */
#pragma once

#include "audio_util.h"
#include "TwoPoleLowPass.h"

class Flange {
	static constexpr int kBufSize = 1024 * (int(0.2 * 2 * signalSampleRate) / 1024); // must be about 1/5 of a second at given sample rate
	const int BOTTOM_FREQ = 100;
public:
	Flange()
		: delayControlFilter(20.0f)
		, mixControlFilter(20.0f) {
		// TODO Auto-generated constructor stub
		// make sure all instance vars are init'd to some known value
		paramSweepRate = 0.2f;
		paramWidth = 0.3f;
		paramFeedback = 0.0f;
		paramDelay = 0.2f;
		sweepRate = 0.2;
		feedback = 0.0;
		feedbackPhase = 1.0;
		sweepSamples = 0;
		setSweep();
		fp = 0;
		sweep = 0.0;
		outval1 = outval2 = 0.0f;
	}

	virtual ~Flange() {}


	void reset() {
		for (int i = 0; i < kBufSize; i++) {
			buf1[i] = 0.0;
		}
		for (int i = 0; i < kBufSize; i++) {
			buf2[i] = 0.0;
		}
	}

	void setParameters(float _rate, float _width, float _fb, float _delay, float _mix) {
		setRate(_rate);
		setWidth(_width);
		setFeedback(_fb);
		setDelay(_delay);
		setMix(_mix);
	}

	void setRate(float rate) {
		paramSweepRate = rate;
		// map into param onto 0.05Hz - 10hz with log curve
		sweepRate = pow(10.0, (double)rate);
		sweepRate -= 1.0;
		sweepRate *= 1.05556f;
		sweepRate += 0.05f;

		// finish setup
		setSweep();
	}
	//	Maps 0.0-1.0 input to calculated width in samples from 0ms to 10ms
	void setWidth(float v) {
		paramWidth = v;
		// map so that we can spec between 0ms and 10ms
		if (v <= 0.05) {
			// eat some noise on the bottom end
			sweepSamples = 0.0;
		}
		else {
			// otherwise calc # of samples for the total width
			sweepSamples = v * 0.01 * signalSampleRate;
		}

		// finish setup
		setSweep();
	}

	void setFeedback(float v) {
		paramFeedback = v;
		feedback = v;
	}

	void setDelay(float v) {
		paramDelay = v;
	}

	void setMix(float v) {
		paramMix = v;
	}


	void apply(float *signal, long sampleFrames)
	{
		for (int i = 0, j = 0; i < sampleFrames; i++, j += 2) {
			// see if we're doing mono
			float inval1, inval2;
			inval1 = signal[j];
			inval2 = signal[j + 1];
			double inmix1 = inval1 + feedback * feedbackPhase * outval1;
			double inmix2 = inval2 + feedback * feedbackPhase * outval2;

			// __android_log_print(ANDROID_LOG_DEBUG, "Flange", "apply() loop %d %d %d %x %x", i, j, fp, (unsigned int)buf1p, (unsigned int)buf2p);
			buf1[fp] = inmix1;
			buf2[fp] = inmix2;
			fp = (fp + 1) & (kBufSize - 1);

			// get filtered delay
			double delay = delayControlFilter(paramDelay);

			// delay 0.0-1.0 maps to 0.02ms to 10ms (always have at least 1 sample of delay)
			double delaySamples = (delay * signalSampleRate * 0.01) + 1.0;
			delaySamples += sweep;

			// build the two emptying pointers and do linear interpolation
			int ep1, ep2;
			double w1, w2;
			double ep = fp - delaySamples;
			if (ep < 0.0) {
				ep += kBufSize;
			}
			w2 = audio_util::modf(ep, ep1);
			ep1 &= (kBufSize - 1);
			ep2 = ep1 + 1;
			ep2 &= (kBufSize - 1);
			w1 = 1.0 - w2;
			outval1 = buf1[ep1] * w1 + buf1[ep2] * w2;
			outval2 = buf2[ep1] * w1 + buf2[ep2] * w2;

			// get filtered mix
			double mix = mixControlFilter(paramMix);
			/*
			double f1 = _mixLeftDry * inval1 + _mixLeftWet * mix * outval1;
			double f2 = _mixRightDry * inval2 + _mixRightWet * mix * outval2;
			*/
			double f1 = inval1 + mix * outval1;
			double f2 = inval2 + mix * outval2;
			f1 = audio_util::pin(f1, -0.999, 0.999);
			f2 = audio_util::pin(f2, -0.999, 0.999);

			// pin to desired output range
			signal[j] = (float)audio_util::pin(f1, -0.99, 0.99);
			signal[j + 1] = (float)audio_util::pin(f2, -0.99, 0.99);

			// see if we're doing sweep
			if (step != 0.0) {
				sweep += step;// increment the sweep
				if (sweep <= 0.0) {
					sweep = 0.0;// make sure we don't go negative
					step = -step; // and reverse direction
				}
				else if (sweep >= maxSweepSamples) {
					step = -step;
				}
			}
		}

	}
protected:
	/*!
	 * Sets up sweep based on rate, depth, and delay as they're all interrelated
	 * Assumes _sweepRate and _sweepSamples have all been set by
	 * setRate and setWidth and calcs the steps to achieve the desired sweep rate
	 */
	void setSweep()
	{
		// calc # of samples per second we'll need to move to achieve spec'd sweep rate
		step = (double)(sweepSamples * 2.0 * sweepRate) / (double)signalSampleRate;

		// calc max and start sweep at 0.0
		sweep = 0.0;
		maxSweepSamples = sweepSamples;
	}

	float paramSweepRate;		// 0.0-1.0 passed in
	float paramWidth;			// ditto
	float paramFeedback;		// ditto
	float paramDelay;			// ditto
	float paramMix;

	double sweepRate;			// actual calc'd sweep rate
	double step;				// amount to step the sweep each sample
	double sweep;				// current value of sweep in steps behind fill pointer
	double feedback;			// 0.0 to 1.0
	double feedbackPhase;		// -1.0 to 1.0
	double sweepSamples;			// sweep width in # of samples
	double maxSweepSamples;	// upper bound of sweep in samples

	double outval1;			// most recent output value (for feedback)
	double outval2;			// most recent output value (for feedback)

	int	   fp;					// fill/write pointer

	std::array<Sample, kBufSize> buf1;
	std::array<Sample, kBufSize> buf2;

	TwoPoleLowPassFilter delayControlFilter; // filters condition these params so they can be driven hard
	TwoPoleLowPassFilter mixControlFilter;
};
