#pragma once

#include <math.h>
#include "AudioSignal.h"

/**
 * TODO: really should come back and look at this
 * still not quite sure what the frequency in the basic version represents ... not the modulator frequency? filter cutoff?
 * a mysterious piece of source code adapted and re-adapted till only fragments of sanity remain.
 */
class RingMod
{
public:
	RingMod( long inFrequency ){
		setFrequency(inFrequency);
		mSi = 0;
	}

	RingMod() { setFrequency(1); }

	~RingMod() {}


	long apply(float *Sig, long nFrames, short nChan);
	bool setParameters(float p[], int np);

	void setFrequency( float inFrequency ){
		if (inFrequency == 0) inFrequency = 1;
		mFrequency = inFrequency;
		mFilterCoefficient = 2.0 * cos( 2.0 * pi / (float) inFrequency );

		mNMinus1 = sin( 2.0 * pi / (float) mFrequency );
		mNMinus2 = 0.0;

		mFink = 2.0 * pi  * mFrequency /nyquist;
	}

	
private:
	float mFrequency;
	
	float mNMinus1;
	float mNMinus2;
	float mFilterCoefficient;
	float mSi;
	float mFink;
};

inline long
RingMod::apply(float *outSig, long nFrames, short nChan)
{
	float nMinus1		= mNMinus1;
	float nMinus2		= mNMinus2;
	float coef			= mFilterCoefficient;
	float temp;

	for ( long n = 0; n < nFrames; n++ )
	{
		temp = coef * nMinus1 - nMinus2;
		nMinus2 = nMinus1;
		nMinus1 = temp;
		/*
		temp = sin(mSi);
		mSi += mFink;
		if (mSi >= 2 * M_PI) {
			mSi -= 2 * M_PI;
		}*/
		for (short i=0;i<nChan;i++) {
//			float v = outSig[nChan*n+i] * temp;
//			clip(v);
			outSig[nChan*n+i] = outSig[nChan*n+i] * temp;
//			if ((n+1)>=inCount) {
//				outSig[i] = (float) outSig[0+i] * temp;
//			} else {
//				outSig[nc*(n+1)*i] = (float) outSig[nc*(n+1)*i] * temp;
//			}
		}
	}

	mNMinus1 = nMinus1;
	mNMinus2 = nMinus2;
	return nFrames;
}

