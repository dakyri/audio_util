#pragma once

#include <math.h>

#include "AudioSignal.h"

class TwoPoleLowPassFilter
{
public:
	inline TwoPoleLowPassFilter(float cutoff) {
		Sample Q = 1.1f;
	    w0 = 2 * pi * cutoff / signalSampleRate;
	    alpha = sin(w0) / (2.0 * Q);
	    b0 =  (1.0 - cos(w0))/2;
	    b1 =   1.0 - cos(w0);
	    b2 =  (1.0 - cos(w0))/2;
	    a0 =   1.0 + alpha;
	    a1 =  -2.0 * cos(w0);
	    a2 = 1.0 - alpha;
		x1 = x2 = y1 = y2 = 0;
	}

	~TwoPoleLowPassFilter() {
	}

	inline Sample operator() (Sample x) {
	    double y = (b0/a0)*x + (b1/a0)*x1 + (b2/a0)*x2 - (a1/a0)*y1 - (a2/a0)*y2;
	    x2 = x1;
	    x1 = x;
	    y2 = y1;
	    y1 = y;
	    return y;
}

private:
        double x1, x2, y1, y2;
        double a0, a1, a2, b0, b1, b2, w0, alpha;
};
