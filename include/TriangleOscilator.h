#pragma once
#include "AudioSignal.h"

class TriangleOscilator
{
public:
	inline TriangleOscilator(float amp = 1.0f, float sr = signalSampleRate) : sampleRate(sr)
	{
		currentValue = peak = amp = amp;
		skew = 0;
		up = false;
		frequency = 0;

		setFrequency(220);
	}

	inline void setSkew(float sk) {
		skew = (sk<0 ? 0 : sk>1 ? 1 : sk);
		setFrequency(frequency, true);
	}

	inline void setAmplitude(float a)  {
		peak = amp = a;
	}

	inline void setFrequency(float freq, bool force = false) {
		if (freq != frequency || force) {
			frequency = freq;

			cycle = (sampleRate / freq);
			int		rx = (1 - skew)*cycle / 2.0;
			if (rx < 1) rx = 1;
			inc_u = (2 * peak) / rx;
			inc_d = (2 * peak) / (cycle - rx);
			inc = up ? inc_u : -inc_d;
		}
	}

	inline void	setPhase(float phs) {
		float sam = (phs / (2 * pi));
		if (sam > 0.5) {
			currentValue = inc_u * (sam - 0.5) - amp;
			up = true;
		} else {
			currentValue = amp - inc_d * sam;
			up = false;
		}
	}

	inline Sample operator()(void) {
		currentValue += inc;
		if (currentValue <= -peak) {
			currentValue = -peak;
			inc = inc_u;
			up = true;
		}
		else if (currentValue >= peak) {
			currentValue = peak;
			inc = -inc_d;
			up = false;
		}
		return currentValue;
	}

	template <uint N> inline void operator () (Signal<N> &outSig) {
		for (unsigned long i = 0; i<maxFramesPerChunk; i++)
			outSig[i] = (*this)();
	}


private:
	float amp;
	float inc_u;
	float inc_d;
	float inc;
	float peak;
	float currentValue;
	long cycle;
	float frequency;
	bool up;
	float skew;
	float sampleRate;
};
