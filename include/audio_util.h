#pragma once

#include <string>
#include <math.h>

typedef int status_t;
typedef unsigned char byte_t;
typedef unsigned long ulong;
typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned char uchar;
typedef float	Sample;

const int OKIDOKI = 0;
const int NOT_OK = -1;
const int kChunkSize = 16 * 1024;
const float pi = 3.14159265358979323846f;
const float pi_2 = (pi / 2.0);
constexpr float signalSampleRate = 44100.0;
const float nyquist = signalSampleRate / 2;

namespace audio_util {

	template <typename T> inline T pin(T n, T min, T max) {	
		return n > max? max : n < min ? min : n;
	}

	template <typename T> inline T modf(T n, int &ip) {
		T tip;
		T fp = ::modf(n, &tip);
		ip = int(tip);
		return fp;
	}

	inline std::string uintstr(uint32_t x) {
		char *p = (char*)&x;
		std::string s;
		s.push_back(p[0]);
		s.push_back(p[1]);
		s.push_back(p[2]);
		s.push_back(p[3]);
		return s;
	}

	inline void clip(float& samp) {	//fastest way I could find to clip a signal
		char x = (char)samp;
		if (x<0) {
			samp = -1.;
		} else if (x > 0) {
			samp = 1.;
		}
	}

	inline void softClip(float &v) {
		if (v > 0.8) {
			v = float((v < 1.6) ? 0.8 + (v - 0.8) / 4 : 1.0);
		} else if (v < -0.8) {
			v = float((v > -1.6) ? -0.8 + (v + 0.8) / 4 : -1.0);
		}
	}

	inline float wideClip(float x) {
		return x > 2 ? 2 : x < -2 ? -2 : x;
	}

	inline void freqCheck(float &f) {
		if (f<1.0) f = 1.0;
		if (f>(signalSampleRate / 2 - 500)) f = (signalSampleRate / 2 - 500);
	}

	inline float
	interpolate(float d1, float d2, float p) {
		return d1 + p*(d2 - d1);
	}

}
#define GLOUB_LITTLE_ENDIAN