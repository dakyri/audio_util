/*
 * LFO.h
 *
 *  Created on: Oct 22, 2013
 *      Author: dak
 */

#pragma once

#include <vector>

#include "Modulation.h"
#include "TimeSource.h"
#include "TableOscilator.h"

const int wavetableLength = 1024;

extern FunctionTable sineWavetable;
extern FunctionTable squareWavetable;
extern FunctionTable posExpWavetable;
extern FunctionTable negExpWavetable;
extern FunctionTable posSawWavetable;
extern FunctionTable negSawWavetable;

class LFO: public TableOscilator
{
public:
	enum Waveform {
		SIN, EXP, NEGEXP, SQUARE, SAW, NEGSAW
	};
	enum Reset {
		NONE, ONFIRE
	};

	LFO(TimeSource &t) : timer(t) {
		setFunkTable(&sineWavetable);
		waveform = Waveform::SIN;
		resetMode = Reset::ONFIRE;
		lock = true;
		rate = 1;
		phase = 0;
 	}
	~LFO() {}

	int waveform;
	unsigned char resetMode;
	unsigned char lock;
	float rate;
	float phase;
	TimeSource &timer;
	float lastValue = 0;
	std::vector<Modulation::Target> target;

	inline void set(int wf, unsigned char res, unsigned char l, float r, float ph) {
		setWave(wf);
		setPhase(ph);
		setRate(r, l);
		setReset(res);
	}

	void setWave(int wf)
	{
		if (wf != waveform) {
			switch (wf) {
			case Waveform::SIN: setFunkTable(&sineWavetable); break;
			case Waveform::EXP: setFunkTable(&posExpWavetable); break;
			case Waveform::NEGEXP: setFunkTable(&negExpWavetable); break;
			case Waveform::SQUARE: setFunkTable(&squareWavetable); break;
			case Waveform::SAW: setFunkTable(&posSawWavetable); break;
			case Waveform::NEGSAW: setFunkTable(&negSawWavetable); break;
			}
			waveform = wf;
		}

	}

	void setPhase(float ph) {
		if (phase != ph) {
			phase = ph;
			TableOscilator::setPhase(phase);
		}
	}

	/**
	 * rate. measured in beats/seconds per cycle
	 */
	void setRate(float r, unsigned char l) {
		if (rate != r || lock != l) {
			lock = l;
			if (r == 0) r = 0.001;
			rate = r;
			if (lock) {
				setRelativeFrequency(timer.beatsPerSec*timer.secsPerCycle / rate);
			}
			else {
				setRelativeFrequency(timer.secsPerCycle / rate);
			}
		}
	}

	void setReset(unsigned char r) {
		resetMode = r;
	}

	inline void resetRate() {
		if (lock) {
			setRelativeFrequency(timer.beatsPerSec*timer.secsPerCycle / rate);
		} else {
			setRelativeFrequency(timer.secsPerCycle / rate);
		}
	}

	inline void modulate(float mod) {
		if (lock) {
			setRelativeFrequency(mod*timer.beatsPerSec*timer.secsPerCycle / rate);
		} else {
			setRelativeFrequency(mod*timer.secsPerCycle / rate);
		}

	}


	inline void reset() {
		TableOscilator::setPhase(phase);
	}
	static constexpr float logRmin = -5;
};

class LFOHost
{
public:
	float envValue(int which) {
		if (which < 0 || which >= lfos.size()) {
			return 0;
		}
		return lfos[which].lastValue;
	}

	int hasEnv(int tgt) {
		for (int i = 0; i<lfos.size(); ++i) {
			for (int j = 0; j<lfos[i].target.size(); ++j) {
				if (lfos[i].target[j].target == tgt) return i;
			}
		}
		return -1;
	}

	LFO &lfo(int which) {
		if (which < 0 || which >= lfos.size()) {
			return lfos[which];
		}
		return LFO(TimeSource());
	}

	void add(LFO &e) {
		lfos.push_back(e);
	}

protected:
	std::vector<LFO> lfos;
};