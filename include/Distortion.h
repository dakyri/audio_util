#pragma once

#include "audio_util.h"

class Distortion
{
	const float distortionNormalizeSoft = 0.25;	//2000
	const float distortionNormalizeTube = 0.25;	//1000
public:
	Distortion() {
		dstAmount = tubeAmount = 0.0;
	}

	/*! Soft clipping: dist = x - 1/3 * x^3	*/
	static inline float dist(float x) {
		return (x - .33333333 * x*x*x);
		//	return (x>1.0)? (.66666667): (x<-1.0)? (-.66666667): (x - .33333333 * x*x*x);
	}

	/*! Tube-ish distortion: dist = (x +.5)^2 -.25 */
	static inline float tube(float x) {
		return ((x + .5)*(x + .5) - .25);
	}
						
	long apply(float *sig, long nFrames, short nChan) {
		long	outSigLen = nFrames * nChan;

		for (short i = 0; i<outSigLen; i++) {
			float dist0 = dstAmount ?
				(1 - dstAmount)*sig[i] + (dstAmount*distortionNormalizeSoft*dist(sig[i] / distortionNormalizeSoft)) : sig[i];
			float dist1 = tubeAmount ?
				(1 - tubeAmount)*dist0 + (tubeAmount*distortionNormalizeTube*tube(dist0 / distortionNormalizeTube)) : dist0;
			sig[i] = audio_util::wideClip(dist1);
		}
		return nFrames;
	}

	void setParameters(float d, float t) {
		dstAmount = d;
		tubeAmount = t;
	}

	float dstAmount;
	float tubeAmount;
};
