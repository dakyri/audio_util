#pragma once

#include <math.h>
#include "audio_util.h"
#include "Lookup.h"

/**
 * exponential envelope from yStart to yEnd, over Duration samples
 * alpha controls the curvature (the more negative the value, the faster
 * envelopeVal approaches yEnd. F. R. Moore - Elements of Computer Music p184.
 * reset restarts the envelope.
 */

class ExpEnvelope
{
protected:
	const ushort TABLE_LENGTH = 512;
	const float FALLOFF = 200.0;


public:
	ExpEnvelope() {
		eMone = exp(1.0f) - 1;
		FunctionTable* ft = new FunctionTable(TABLE_LENGTH, [=](int i) {
			return 1. - exp(((float)(i - (TABLE_LENGTH / 2.)) / (TABLE_LENGTH / 2.)) *FALLOFF);
		});
		emap = new BoundedLookup(ft, -200, 200);

		FunctionTable* gt = new FunctionTable(TABLE_LENGTH, [=](int i) {
			return log((((float)i / TABLE_LENGTH)*FALLOFF + 1));
		});
		amap = new BoundedLookup(gt, 0, 200);

		reset();
		setAttackAlpha(200);
		setAttackTime(0);
		setHoldTime(0);
		setDecayTime(2);
		setDecayAlpha(-7);
	}
	~ExpEnvelope() {}
						
	inline ExpEnvelope& setAttackTime(unsigned long duration) {
		attackTime = duration;
		return *this;
	}
	inline ExpEnvelope& setHoldTime(unsigned long duration) {
		holdTime = duration;
		return *this;
	}
	inline ExpEnvelope& setDecayTime(unsigned long duration) {
		decayTime = duration;
		return *this;
	}

	inline ExpEnvelope& setDecayAlpha(float alpha) {
		this->d_alpha = alpha;
		//onemEAlpha=1.-exp(d_alpha);
		onemEAlpha = (*emap)[d_alpha];
		return *this;
	}
	inline ExpEnvelope& setAttackAlpha(float alpha) {
		a_alpha = alpha;
		onepLAlpha = (*amap)[a_alpha];
		return *this;
	}
	inline ExpEnvelope& setEndpoints(float start,float end) {
		yStart = start;
		yEndmStart = end - start;
		return *this;
	}
	inline ExpEnvelope& reset(void) {
		curTime = 0;
		return *this;
	}
	
	inline float operator()(void) {
		unsigned		segTime = curTime;
		if (segTime < attackTime) {
			// can't do this with the current maps... us it worth it?
			//		theEnv =  yStart*(exp(((float)segTime)/((float)attackTime))-1)/
			//							EMone;
			theEnv = yStart*(((float)segTime) / ((float)attackTime)); // TODO: should maybe be an exponential
		}
		else {
			segTime -= attackTime;
			if (segTime < holdTime || holdTime < 0) {
				theEnv = yStart;
			}
			else {
				segTime -= holdTime;
				if (segTime < decayTime) {
					theEnv = yStart +
						yEndmStart*
						(((*emap)[(segTime / (float)decayTime)*d_alpha]) / onemEAlpha);
				}
				else {
					theEnv = yStart + yEndmStart;
				}
			}
		}
		curTime++;
		return theEnv;
	}


private:
	float theEnv;

	ulong curTime;
	
	float yStart,yEndmStart;
	float sustainLevel;
	float d_alpha;
	float a_alpha;
	
	ulong attackTime;
	ulong holdTime;
	ulong decayTime;
	
	float onemEAlpha; //(1.-exp(alpha)
	float onepLAlpha; //(1.+exp(alpha)
	float eMone;
	BoundedLookup *emap;
	BoundedLookup *amap;
};
