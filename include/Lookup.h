#pragma once

#include "FunctionTable.h"

/*!
 * Lookup performs interpolated lookup on table, index must range from 0 to table->Length - 1
 * out of range values are not advissed
 */
class Lookup
{
public:
	Lookup( FunctionTable* table=0 ): lookuptable(table) {}
				
	Lookup& setFunkTable(FunctionTable* table) {
		lookuptable = table;
		return *this;
	}
	
	/* index from 0 to size of table */
	float operator [] (float index) {
		unsigned long idx = (unsigned long)index;
		float frac = index - idx;
		return (*lookuptable)[idx] + ((*lookuptable)[idx + 1] - (*lookuptable)[idx])*frac;
	}

protected:
	FunctionTable *lookuptable;
};

/*!
 * BoundedLookup: index values between minimum and maximum map to the range of the
 * ftable. index values outside that range map to the edges of the table.
 */
class BoundedLookup
{
public:
	inline BoundedLookup(FunctionTable* t = nullptr, float dmin = 0., float dmax = 1.) : lookuptable(t) { setLookupRange(dmin, dmax); }				
	inline BoundedLookup& setFunkTable(FunctionTable* table) {
		lookuptable = table;
		if (lookuptable) maxMmindtl = (maximum - minimum) / lookuptable->Length();
		return *this;
	}
	BoundedLookup&	setLookupRange(float dmin, float dmax) {
		minimum = dmin;
		maximum = dmax;
		if (lookuptable) maxMmindtl = (dmax - dmin) / lookuptable->Length();
		return *this;
	}
	
	inline float operator [] (float index /* index from minimum to maximum */) {
		float didx = (index - minimum) / maxMmindtl;
		long idx = (unsigned long)didx;
		float frac = didx - idx;

		if (idx < 0) {
			return (*lookuptable)[0];
		} else if (idx > (int)lookuptable->Length()) { // return the last value in the lookup table
			return (*lookuptable)[lookuptable->Length()];
		}
		return (*lookuptable)[idx] + ((*lookuptable)[idx + 1] - (*lookuptable)[idx])*frac;
	}

protected:
	float minimum,maximum;
	float maxMmindtl;
	FunctionTable *lookuptable;
};

/*!
 * WrappingLookup: index values outside minimum - maximum wrap around.
 */
class WrappingLookup
{
public:
	WrappingLookup(FunctionTable* table=nullptr,float dmin =0.,float dmax =1.) : lookuptable(table) { setLookupRange(dmin, dmax); }
				
	WrappingLookup& setFunkTable(FunctionTable* table) {
		lookuptable = table;
		if (lookuptable) maxMmindtl = (maximum - minimum) / lookuptable->Length();
		return *this;
	}

	WrappingLookup&	setLookupRange(float dmin, float dmax) {
		minimum = dmin;
		maximum = dmax;
		if (lookuptable) maxMmindtl = (dmax - dmin) / lookuptable->Length();
		return *this;
	}

	inline float operator [] (float index) {
		float didx = (index - minimum) / maxMmindtl;
		long idx = (unsigned long)didx;
		float frac = didx - idx;
		while (idx < 0)
			idx += lookuptable->Length();
		while (idx >= (int)lookuptable->Length())
			idx -= lookuptable->Length();
		return (*lookuptable)[idx] + ((*lookuptable)[idx + 1] - (*lookuptable)[idx])*frac;
	}

protected:
	float minimum,maximum;
	float maxMmindtl;
	FunctionTable *lookuptable;
};