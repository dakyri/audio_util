#pragma once

#include <functional>
#include "audio_util.h"

/*!
 * table of function values for the Lookup and TableOscilator classes.
 * will either be a table of function values or perhaps sample points
 * values range from 0..length
 */
class FunctionTable
{
public:
	FunctionTable(ulong len): length(len) {
		data = new float[length + 1];
	}
	FunctionTable(ulong len, std::function<float(int)> f) : FunctionTable(len) {
		for (ulong i = 0; i <= len; ++i) {
			data[i] = f(i);
		}
	}
	~FunctionTable() {
		delete [] data;
	}
				
	inline float& operator [] (ulong i) const {
		return data[i];
	}
	
	ulong Length() {
		return length;
	}

private:
	ulong length; // actual array length - 1
	float *data;
};