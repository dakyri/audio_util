#pragma once
/*
 * Modulation.h
 *
 *  Created on: Oct 22, 2013
 *      Author: dak
 */
class Modulation {
public:
	enum Mode {
		ResetAtEnd = 0
	};
	struct Target {
		Target(int t = 0, float a = 0) : target(t), amount(a) {}
		int target;
		float amount;
	};

};
