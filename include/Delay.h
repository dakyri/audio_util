#pragma once

#include <vector>

#include "audio_util.h"

class Delay {
public:
	Delay(int nc, float maxDelaySecs) {
		nChannel = nc;
		setDelayBuffer(maxDelaySecs);
		inputIndex = 0;
	 }
	virtual ~Delay() {
		delete [] delayBuffer;
	 }

	inline void reset() {
		for (ulong i = 0; i <= length; i++)
			delayBuffer[i] = 0.;
	}

	void setDelayBuffer(float maxDelaySecs) {
		if (delayBuffer) delete[] delayBuffer;
		auto maxDelayFrames = ceil(maxDelay*signalSampleRate);
		length = (ulong)(maxDelayFrames*nChannel);
		delayBuffer = new float[length + nChannel];
	}

protected:
	static constexpr float kMinDelay = 0.000001;

	inline void write(float * inSig, int inSigLen, float gain, bool inc=true)
	{
		int cnt = inSigLen;
		while (cnt > 0) {
			int cnti = (inputIndex + cnt > length ? length - inputIndex : cnt);
			cnt -= cnti;
			for (unsigned long i = 0; i<cnti; i++) {
				buffPointer[inputIndex + i] = gain*inSig[i];
			}
			//write guard point
			if (inputIndex == 0) {
				for (short i = 0; i<nChannel; i++)
					buffPointer[length + i] = buffPointer[0 + i];
			}
			if (inc) {
				inputIndex += cnti;
				if (inputIndex >= length) {
					inputIndex = 0;
				}
			}
		}
	}

	inline void overWrite(float * inSig, int inSigLen, float gain)
	{
		int cnt = inSigLen;
		while (cnt > 0) {
			int cnti = (inputIndex + cnt > length ? length - inputIndex : cnt);
			cnt -= cnti;
			for (unsigned long i = 0; i<cnti; i++) {
				buffPointer[inputIndex + i] += gain*inSig[i];
			}

			//write guard point
			if (inputIndex == 0) {
				for (short i = 0; i<nChannel; i++)
					buffPointer[length + i] = buffPointer[0 + i];
			}
			inputIndex += cnti;
			if (inputIndex >= length)
				inputIndex = 0;
		}
	}

	inline void read(float *outSig, int outSigLen, double delaySecs)
	{
		double fReadIndex = inputIndex - nChannel* delaySecs * signalSampleRate;
		while (fReadIndex<0)
			fReadIndex += length;

		unsigned long dReadIndex = (((unsigned long)fReadIndex) / nChannel)*nChannel;
		double frac = fReadIndex - dReadIndex;
		for (unsigned long i = 0; i<outSigLen; i++) {
			outSig[i] = buffPointer[dReadIndex] +
				(buffPointer[dReadIndex + nChannel] - buffPointer[dReadIndex]) *frac;

			dReadIndex++;
			if (dReadIndex >= length) {
				dReadIndex -= length;
			}
		}
	}

	float maxDelay;
	uint length;
	ushort nChannel;
	uint inputIndex;
	Sample *delayBuffer;
	float* buffPointer;	//multiple of vLen with extended guard
};

class EchoDelay: public Delay {
public:
	EchoDelay(int nc, float maxDelaySecs) : Delay(nc, maxDelaySecs) {
		dlTime = 1;
		dlEchoVolume = 0.5;
		dlFeedback = 0;
	}

	inline int apply(float *outSig, int nFrames) {
		std::vector<float> tempSig1(nFrames * nChannel);
		long outSigLen = nFrames * nChannel;

		read(tempSig1.data(), outSigLen, dlTime);		// (60./ mySeq.seq.tempo) / dtFactor);
		write(outSig, outSigLen, 1 - dlFeedback, false);

		for (short i = 0; i<outSigLen; i++) {
			outSig[i] += tempSig1[i] * dlEchoVolume;
		}
		overWrite(outSig, outSigLen, dlFeedback);

		//	__android_log_print(ANDROID_LOG_DEBUG, "EchoDelay", "done %d %d <- %d", inputIndex, length, nFrames);
		return nFrames;
	}

	void setGain(float v) { dlEchoVolume = (v<0 ? 0 : v>1 ? 1 : v); }
	void setTime(float t) { dlTime = (t<kMinDelay ? kMinDelay : t > maxDelay ? maxDelay : t); }
	void setFeedback(float fb) { dlFeedback = (fb<0 ? 0 : fb>1 ? 1 : fb); }


protected:
	float dlTime;
	float dlEchoVolume;
	float dlFeedback;
};


class FBDelay: public Delay {
public:
	FBDelay(int nc, float maxDelaySecs) : Delay(nc, maxDelaySecs) {
		dlTime = 1;
		dlFeedback = 0;
	}

	inline int apply(float *sig, int nFrames)
	{
		std::vector<float> tempLrSig(nChannel*nFrames);
		int outLen = nChannel*nFrames;
		if (dlFeedback > 0) {
			read(tempLrSig.data(), outLen, dlTime);
			for (short i = 0; i<outLen; i++) {
				sig[i] += tempLrSig[i] * dlFeedback;
			}
		}
		write(sig, outLen, 1);

		return nFrames;
	}

	void setGain(float fb) { dlFeedback = (fb<0 ? 0 : fb>1 ? 1 : fb); }
	void setTime(float t) { dlTime = (t<kMinDelay ? kMinDelay : t > maxDelay ? maxDelay : t); }

protected:
	float dlTime;
	float dlFeedback;
};

class SideChainDelay: public Delay {
public:
	SideChainDelay(int nc, float maxDelaySecs) : Delay(nc, maxDelaySecs) {
		dlTime = 1;
		dlEchoVolume = 0.5;
		dlFeedback = 0;
	}


	inline int apply(float *outSig, float *sideSig, int nFrames)
	{
		std::vector<float> tempSig1(nFrames * nChannel);
		long outSigLen = nFrames * nChannel;

		read(tempSig1.data(), outSigLen, dlTime);		// (60./ mySeq.seq.tempo) / dtFactor);
		write(outSig, outSigLen, 1 - dlFeedback, false);

		for (short i = 0; i<outSigLen; i++) {
			sideSig[i] += tempSig1[i] * dlEchoVolume;
		}
		overWrite(sideSig, outSigLen, dlFeedback);

		return nFrames;
	}

	void setGain(float v) { dlEchoVolume = (v<0 ? 0 : v>1 ? 1 : v); }
	void setTime(float t) { dlTime = (t<kMinDelay ? kMinDelay : t > maxDelay ? maxDelay : t); }
	void setFeedback(float fb) { dlFeedback = (fb<0 ? 0 : fb>1 ? 1 : fb); }


protected:
	float dlTime;
	float dlEchoVolume;
	float dlFeedback;
};