/*
 * Chorus.h
 *
 *  Created on: Dec 28, 2014
 *      Author: dak
 */
#pragma once
#include <math.h>
#include <array>
#include "audio_util.h"

class Chorus {
public:
	Chorus() {
		paramSweepRate = 0.2f;
		paramWidth = 0.3f;
		paramFeedback = 0.0f;
		paramDelay = 0.2f;
		mix = 0.5f;
		sweepRate = 0.2;
		feedback = 0.0;
		feedbackPhase = 1.0;
		sweepSamples = 0;
		delaySamples = 22;
		setSweep();
		fp = 0;
		sweep = 0.0;
	}
	virtual ~Chorus() { }

	void initialize() {
		reset();
	}

	void reset() {
		for (int i = 0; i < kBufSize; i++) {
			buf[i] = 0.0;
		}
	}

	void apply(Sample *signal, long sampleFrames)
	{
		for (int i = 0, j = 0; i < sampleFrames; i++, j += 2) {
			// assemble mono input value and store it in circle queue
			float in1 = signal[j];
			float in2 = signal[j + 1];

			float inval = (in1 + in2) / 2.0f;
			buf[fp] = inval;
			fp = (fp + 1) & (kBufSize - 1);

			// build the two emptying pointers and do linear interpolation
			int ep1, ep2;
			double w1, w2;
			double ep = fp - sweep;
			w2 = audio_util::modf(ep, ep1);
			ep1 &= (kBufSize - 1);
			ep2 = ep1 + 1;
			ep2 &= (kBufSize - 1);
			w1 = 1.0 - w2;
			double outval = buf[ep1] * w1 + buf[ep2] * w2;
			signal[j] = (float)audio_util::pin(in1 + mix*outval, -0.99, 0.99);
			signal[j + 1] = (float)audio_util::pin(in2 + mix*outval, -0.99, 0.99);

			// increment the sweep
			sweep += step;
			if (sweep >= maxSweepSamples || sweep <= minSweepSamples) {
				step = -step;
			}

		}

	}


	/*!
	 * Expects input from 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0
	 */
	void setDelay(float v) {
		paramDelay = v;
		// make onto desired values applying log curve
		double delay = pow(10.0, (double)v * 2.0) / 1000.0;		// map logarithmically and convert to seconds
		delaySamples = round(delay * signalSampleRate);
		setSweep();
	}

	void setRate(float rate) {
		paramSweepRate = rate;
		// map into param onto desired sweep range with log curve
		sweepRate = pow(10.0, (double)paramSweepRate);
		sweepRate -= 1.0;
		sweepRate *= 1.1f;
		sweepRate += 0.1f;

		// finish setup
		setSweep();
	}


	/*!
	 *	Maps 0.0-1.0 input to calculated width in samples from 0ms to 50ms
	 */
	void setWidth(float v) {
		paramWidth = v;
		// map so that we can spec between 0ms and 50ms
		sweepSamples = round(v * 0.05 * signalSampleRate);
		// finish setup
		setSweep();
	}

	void setFeedback(float v) {
		paramFeedback = v;
		feedback = v;
	}


	void setMix(float v) {
		mix = v;
	}

	/*!
	* Sets up sweep based on rate, depth, and delay as they're all interrelated
	* Assumes _sweepRate, _sweepSamples, and _delaySamples have all been set by
	* setRate, setWidth, and setDelay
	*/
	void setSweep()
	{
		// calc # of samples per second we'll need to move to achieve spec'd sweep rate
		step = (double)(sweepSamples * 2.0 * sweepRate) / (double)signalSampleRate;
		if (step <= 1.0) {
			printf("_step out of range: %f\n", step);
		}

		// calc min and max sweep now
		minSweepSamples = delaySamples;
		maxSweepSamples = delaySamples + sweepSamples;

		// set intial sweep pointer to midrange
		sweep = (minSweepSamples + maxSweepSamples) / 2;
	}

	void setParameters(float _rate, float _width, float _fb, float _delay, float _mix) {
		setRate(_rate);
		setWidth(_width);
		setFeedback(_fb);
		setDelay(_delay);
		setMix(_mix);
	}

	static constexpr int kBufSize = 1024 * (int(0.2 * 2 * signalSampleRate) / 1024); // must be about 1/5 of a second at given sample rate
protected:
	float paramSweepRate;		// 0.0-1.0 passed in
	float paramWidth;			// ditto
	float paramFeedback;		// ditto
	float paramDelay;			// ditto
	float mix;		// ditto
	double sweepRate;			// actual calc'd sweep rate
	double feedback;			// 0.0 to 1.0
	double feedbackPhase;		// -1.0 to 1.0
	int sweepSamples;			// sweep width in # of samples
	int delaySamples;		// number of samples to run behind filling pointer
	double minSweepSamples;	// lower bound of calculated sweep range, calc'd by setSweep from rate, width, delay
	double maxSweepSamples;	// upper bound, ditto
	int mixMode;				// mapped to supported mix modes
	int fp;					// fill/write pointer
	double step;				// amount to step the sweep each sample
	double sweep;
	std::array<Sample, kBufSize> buf;	/*!< stored sound */
};