#pragma once

#include "AudioSignal.h"
#include "FunctionTable.h"

/*!
 * oscilator based on a given function table
 *
 * the oscilator retains ownership of the wave table
 *
 * the wavetable should be non-null by the time initialization is done
 *
 */
class TableOscilator {	
public:
	TableOscilator(FunctionTable *wavetable=nullptr, float phase = 0.)
		: phs(phase), wavetable(wavetable) { si = 1; }
	~TableOscilator() { if (wavetable)  delete wavetable; }
					
	TableOscilator& setFunkTable(FunctionTable *wt) {
		if (wavetable) delete wavetable;
		wavetable = wt;
		return *this;
	}

	TableOscilator& setPhase(float phase) { //0 <= phase <=1.
		phs = phase*wavetable->Length();
		return *this;
	}

	TableOscilator& setFrequency(float freq, float sampleRate = signalSampleRate) {
		si = ((float)wavetable->Length())*freq / sampleRate;
		return *this;
	}

	TableOscilator& setRelativeFrequency(float rFreq) {
		si = ((float)wavetable->Length())*rFreq;
		return *this;
	}
	
	inline Sample operator () (void) {
		ulong idx = (ulong)phs;
		float frac = phs - idx;
		phs += si;
		if (phs>(int)wavetable->Length())
			phs -= wavetable->Length();

		return (*wavetable)[idx] + ((*wavetable)[idx + 1] - (*wavetable)[idx])*frac;
	}


	template <uint N> inline void operator () (Signal<N> &outSig) {
		for (unsigned long i = 0; i< outSig.length; i++)
			outSig[i] = operator()();
	}

	inline Sample operator *(void) {
		ulong idx = (ulong)phs;
		float frac = phs - idx;
		return (*wavetable)[idx] + ((*wavetable)[idx + 1] - (*wavetable)[idx])*frac;
	}

protected:
	float phs; //0. to wavetable->Length;
	float si;
	FunctionTable *wavetable;
};
