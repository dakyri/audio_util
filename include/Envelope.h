#pragma once
/*
 * Envelope.h
 *
 *  Created on: Oct 21, 2013
 *      Author: dak
 */

#include <vector>
#include "TimeSource.h"
#include "Modulation.h"

class Envelope
{
public:
	Envelope(float attackT2, float decayT2, float sustainL2, float sustainT2, float releaseT2, TimeSource &t) : timer(t) {
		set(attackT2, decayT2, sustainL2, sustainT2, sustainT2);
		atEnd = false;
		lastValue = 0;
	}

	Envelope(TimeSource &t): Envelope(0, 0, 1, 0, 0, t) {}

	~Envelope() {}

	float attackT;
	float decayT;
	float sustainT;
	float sustainL;
	float releaseT;

	float lastValue;

	unsigned char resetMode;
	unsigned char lock;
	TimeSource &timer;
	bool atEnd;

	std::vector<Modulation::Target> target;

	inline void set(float attackT2, float decayT2, float sustainL2, float sustainT2, float releaseT2)
	{
		attackT = attackT2;
		decayT = decayT2;
		sustainT = sustainT2;
		sustainL = sustainL2;
		releaseT = releaseT2;
	}

	inline float operator()(void) {
		float segT = envTime*timer.secsPerCycle;
		envTime++;
		if (lock) {
			segT *= timer.secsPerBeat;
		}
		float v = 0;
		if (segT < attackT) {
			v = segT / attackT;
		}
		else if ((segT -= attackT) < decayT) {
			v = 1 - ((1 - sustainL)*segT / decayT);
		}
		else if ((segT -= decayT) < sustainT) {
			v = sustainL;
		}
		else if ((segT -= sustainT) < releaseT) {
			v = sustainL*(1 - (segT / releaseT));
		}
		else {
			if (resetMode == Modulation::Mode::ResetAtEnd) {
				envTime = 0;
			}
			else {
				atEnd = true;
			}
		}
		lastValue = v;

		return v;
	}

	inline void reset() {
		envTime = 0;
		lastValue = 0;
		atEnd = false;
	}


protected:
	unsigned long envTime;
};

class EnvelopeHost
{
public:
	float envValue(int which) {
		if (which < 0 || which >= env.size()) {
			return 0;
		}
		return env[which].lastValue;
	}

	int hasEnv(int tgt) {
		for (int i = 0; i<env.size(); ++i) {
			for (int j = 0; j<env[i].target.size(); ++j) {
				if (env[i].target[j].target == tgt) return i;
			}
		}
		return -1;
	}

	Envelope &envelope(int which) {
		if (which < 0 || which >= env.size()) {
			return env[which];
		}
		return Envelope(TimeSource());
	}

	void add(Envelope &e) {
		env.push_back(e);
	}

protected:
	std::vector<Envelope> env;
};